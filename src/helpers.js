export function getMovingAvg(arr){
    const returnArr = [...arr];
    for(let i = 0; i < returnArr.length; i++){
        let counter = 0;
        let sum = 0;
        for(let j = -5; j < 6; j++){
            if(i - j >= 0 && i - j < returnArr.length){
                sum += returnArr[i - j];
                counter++;
            }
        }
        returnArr[i] = sum/counter;
    }
    return returnArr;
}